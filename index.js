const fs = require("fs");
const http = require("http");

/**
 * HTTP server which shares proto file.
 * @class
 */
class SolarHTTP {

	/**
	 * Creates new instance of SolarHTTP.
	 * @constructor
	 * @param {String} config.protoPath Path to .proto file
	 * @param {Number=} config.port Port, default: process.env.HTTP_PORT || 3000
	 * @param {String=} config.host Host, default: process.env.HTTP_HOST || "localhost"
	 * @param {String=} config.endpoint Url for GET endpoint that shares .proto file, default: "/proto"
	 */
	constructor(config) {
		if (!config.protoPath) {
			throw new Error("Proto path not specified");
		}
		this.protoPath = config.protoPath;
		this.port = config.port || process.env.HTTP_PORT || 3000;
		this.host = config.host || process.env.HTTP_HOST || "localhost";
		this.endpoint = config.endpoint || process.env.HTTP_ENDPOINT || "/proto";
	}

	/**
	 * Starts SolarHTTP server.
	 *
	 * @example Start server with ./example.proto
	 * const httpServer = new SolarHTTP({ protoPath: "./example.proto" });
	 *
	 * @example Start server with ./example.proto on specific port
	 * const httpServer = new SolarHTTP({ protoPath: "./example.proto", port: 9000 });
	 */
	start() {
		return http.createServer((req, res) => {
			if (req.url === this.endpoint && req.method === "GET") {
				const fileStat = fs.statSync(this.protoPath);

				res.writeHead(200, {
					"Content-Type": "text/proto3", "Content-Length": fileStat.size
				});
				fs.createReadStream(this.protoPath).pipe(res);
			} else {
				res.writeHead(404);
				res.end();
			}
		}).listen(this.port, this.host, () => {
			console.log(`HTTP Service is up and running on ${this.host}:${this.port}`);
			console.log(`Proto file can be accessed via GET request ON ${this.endpoint}`);
		});
	}
}

module.exports = SolarHTTP;